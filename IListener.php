<?php

namespace HotWire\EventDispatcher;

interface IListener
{
    /**
     * add subscriber
     * @param ISubscriber $subscriber
     */
    public function add(ISubscriber $subscriber);

    /**
     * remove subscriber
     * @param  ISubscriber $subscriber
     * @return self
     */
    public function remove(ISubscriber $subscriber);

    /**
     * dispatch event
     * @param  string      $eventName
     * @param  IEvent      $event
     * @return ISubscriber
     */
    public function dispatch($eventName,IEvent $event);
}
