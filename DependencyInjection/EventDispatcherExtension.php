<?php

namespace HotWire\EventDispatcher\DependencyInjection;

use HotWire\DependencyInjection\Extension;
use HotWire\EventDispatcher\Listener;

class EventDispatcherExtension extends Extension
{
    /**
     * register listener as server
     */
    public function load()
    {
        $this->container->register('event.dispatcher', new Listener());
    }
}
