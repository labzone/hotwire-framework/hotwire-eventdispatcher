<?php

namespace HotWire\EventDispatcher;

use HotWire\Framework\AbstractApp;

class App extends AbstractApp
{
    /**
     * get app name
     * @return string
     */
    public function getName()
    {
        return 'HotWire:EventDispatcher';
    }
}
