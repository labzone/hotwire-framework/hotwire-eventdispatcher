<?php

namespace HotWire\EventDispatcher;

class Listener implements IListener
{
    /**
     * subscriber callbacks
     * @var array
     */
    private $subscriberCallbacks=array();

    /**
     * add subscriber
     * @param ISubscriber $subscriber
     */
    public function add(ISubscriber $subscriber)
    {
        foreach ($subscriber->getSubscribedEvents() as $name => $callback) {
            $this->subscriberCallbacks[$name][]=get_class($subscriber).':'.$callback;
        }

        return $this;
    }

    /**
     * remove subscriber
     * @param  ISubscriber $subscriber
     * @return self
     */
    public function remove(ISubscriber $subscriber)
    {
        foreach ($subscriber->getSubscribedEvents() as $name => $callback) {
            if ($key=array_search($name, $this->subscriberCallbacks)) {
                unset($this->subscriberCallbacks[$key]);
            }
        }

        return $this;
    }

    /**
     * dispatch event
     * @param  string      $eventName
     * @param  IEvent      $event
     * @return ISubscriber
     */
    public function dispatch($eventName,IEvent $event)
    {
        if (isset($this->subscriberCallbacks[$eventName])) {
            $subscriberCallbacks=$this->subscriberCallbacks[$eventName];
            ksort($subscriberCallbacks);
            foreach ($subscriberCallbacks as $priority => $callback) {
                if (is_callable($callback)) {
                    call_user_func($callback, $event);
                } else {
                    self::callSubscriber($callback, $event);
                }
            }
        }
    }

    /**
     * call subscriber
     * @param string $callback
     * @param IEvent $event
     */
    private static function callSubscriber($callback,IEvent $event)
    {
        $keys=array('class','method');
        $items=explode(':', $callback);
        $callable=array_combine($keys, $items);
        if ($reflectionClass=new \ReflectionClass($callable['class'])) {
            if ($reflectionMethod=$reflectionClass->getMethod($callable['method'])) {
                $reflectionMethod->invoke(new $callable['class'], $event);
            }
        }
    }
}
