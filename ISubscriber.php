<?php

namespace HotWire\EventDispatcher;

interface ISubscriber
{
    /**
     * get subscribed events
     * @return array
     */
    public function getSubscribedEvents();
}
