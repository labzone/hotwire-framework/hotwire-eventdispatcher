<?php

namespace HotWire\EventDispatcher;

interface IEvent
{
    /**
     * set data
     * @param mixed $data
     */
    public function setData($data);

    /**
     * get data
     * @return mixed
     */
    public function getData();
}
